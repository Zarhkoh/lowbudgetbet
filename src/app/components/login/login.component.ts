import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';
import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error;
  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit(): void {
  }

  loginForm: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  submit() {
    this.error = "";
    if (this.loginForm.valid) {
      this.loginService
        .login(this.loginForm.value)
        .subscribe(data => this.loginSuccess(data), error => this.error = error.error);
    } else {
      this.error = "Merci de remplir tous les champs.";
    }
  }

  loginSuccess(data) {
    console.log(data);
    localStorage.setItem('userToken', JSON.stringify(data));
    this.error = "LOGIN SUCCESS";
    this.router.navigate(['/profil']);
  }
}
