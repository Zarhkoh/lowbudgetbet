export class User {
    id: number;
    nom: string;
    prenom: string;
    dateNaissance: Date;
    email: string;
    pseudo: string;
    avatarUrl: string;
    password: string;
    parisGagnes: number;
    parisPerdus: number;
    iban: string;
    role: string;
}