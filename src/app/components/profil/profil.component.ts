import { Component, OnInit } from '@angular/core';
import { ProfilService } from 'src/app/services/profil.service';
import { User } from '../models/user';


@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  user: User;
  constructor(private profilService: ProfilService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('userToken'));
    this.refreshUserInformations(this.user.id);
  }

  refreshUserInformations(userId) {
    this.profilService.getUserInformations(userId).subscribe(data => this.user = data as User, error => console.log(error));
  };
}
