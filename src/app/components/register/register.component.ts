import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  constructor(private registerService: RegisterService, private router: Router) { }


  error;
  success;

  registerForm: FormGroup = new FormGroup({
    nom: new FormControl(''),
    prenom: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    passwordConfirm: new FormControl(''),
    pseudo: new FormControl(''),
    parrain: new FormControl(''),
    dateNaissance: new FormControl(''),
  });

  ngOnInit(): void {
  }

  submit() {
    this.error = "";
    this.success = "";
    if (this.registerForm.valid && this.registerForm.get('password').value == this.registerForm.get('passwordConfirm').value) {
      this.registerService
        .register(this.registerForm.value)
        .subscribe(data => this.registerSuccess(data), error => this.error = error.error);
    } else {
      if (this.registerForm.valid && this.registerForm.get('password') != this.registerForm.get('passwordConfirm')) {
        this.error = "Les mots de passe sont différent";
      } else this.error = "Merci de remplir tous les champs.";
    }
  }

  registerSuccess(data) {
    localStorage.setItem('token', data);
    this.success = "Enregistrement réussi, veuillez vous connecter.";
    // this.router.navigate(['/profile']);
  }
}
