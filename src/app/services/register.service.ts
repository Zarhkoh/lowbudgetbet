import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../components/models/user';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  url = "http://localhost:8083/users";

  constructor(private http: HttpClient) { }

  register(user: User) {
    user.avatarUrl = "https://avatars.dicebear.com/api/avataaars/" + String(Math.floor(Math.random() * 99879879879879797898797)) + ".svg";
    return this.http.post(`${this.url}/register`, user);
  }
}
