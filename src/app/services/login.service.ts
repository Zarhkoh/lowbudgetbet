import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../components/models/user';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  url = "http://localhost:8083/users";

  constructor(private http: HttpClient) { }

  login(login: User) {
    return this.http.post(`${this.url}/login`, login);
  }

  logout() {
    localStorage.removeItem('token');
    return !localStorage.getItem('token');
  }
}
