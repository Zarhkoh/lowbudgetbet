import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProfilService {
  url = "http://localhost:8083/users";

  constructor(private http: HttpClient) { }

  getUserInformations(userId: number) {
    return this.http.get(`${this.url}/user/` + userId);
  }

  logout() {
    localStorage.removeItem('token');
    return !localStorage.getItem('token');
  }
}
